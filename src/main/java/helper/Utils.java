package helper;

import logger.Logger;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Utils {
    public static String getCurrentDateTime(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
       return (dtf.format(now));
    }

    public static int getAge(String dob){
        try {
            int yearOfBirth = new SimpleDateFormat("dd/MM/yyyy").parse(dob).getYear();
            int currentYear = LocalDateTime.now().getYear();
            return currentYear-yearOfBirth;

        }
        catch (Exception e){
            Logger.LogInfo("Error while parsing date of birth", e);
            return 18;
        }
    }

    public static Date getDate(String input) {
        try {
           return new SimpleDateFormat("dd/MM/yyyy").parse(input);
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            //return sdf.format(date);
        } catch (Exception e) {
            Logger.LogInfo("Error while parsing date", e);
            return null;
        }
    }
}

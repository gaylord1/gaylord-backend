package helper;

import logger.Logger;

public class ParseHelper {
    public static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException n) {
            Logger.LogInfo("Error while parsing to int", n);
            return false;
        }
    }
}

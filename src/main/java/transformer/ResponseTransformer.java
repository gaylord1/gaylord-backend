package transformer;

public interface ResponseTransformer {
    String render(Object model) throws Exception;
}

package transformer;

import com.google.gson.Gson;
import logger.Logger;

public class GsonTransformer {

    public static <T> T toJson(Class<T> type, String input) {
        try {
            Gson g = new Gson();
            T t = type.newInstance();
            Class<?> zclass = t.getClass();
            return (T) g.fromJson(input, zclass);
        } catch (Exception e) {
            Logger.LogInfo("Error while converting to JSON", e);
            return null;
        }
    }
}

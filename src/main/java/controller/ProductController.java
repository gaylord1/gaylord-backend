package controller;

import entities.Product;
import errormapper.ResponseError;
import helper.ParseHelper;
import service.ProductService;

import static spark.Spark.*;
import static utils.JsonUtil.json;


public class ProductController {
    public ProductController(final ProductService productService) {
//        options("/*",
//                (request, response) -> {
//
//                    String accessControlRequestHeaders = request
//                            .headers("Access-Control-Request-Headers");
//                    if (accessControlRequestHeaders != null) {
//                        response.header("Access-Control-Allow-Headers",
//                                accessControlRequestHeaders);
//                    }
//
//                    String accessControlRequestMethod = request
//                            .headers("Access-Control-Request-Method");
//                    if (accessControlRequestMethod != null) {
//                        response.header("Access-Control-Allow-Methods",
//                                accessControlRequestMethod);
//                    }
//
//                    return "OK";
//                });
//        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));

        get("/ping", (req, res) -> "{\"message\":\"pong\"}", Object::toString);
        get("/products", ((request, response) -> productService.getAllProducts()), json());

        get("/product/:id", (request, response) -> {
            String id = request.params(":id");
            if (!ParseHelper.isInteger(id)) {
                response.status(400);
                return new ResponseError("Please enter a valid product id");
            }
            Product product = productService.getProduct(id);
            if (product != null)
                return product;
            response.status(404);
            return new ResponseError("No product with id %s found", id);
        }, json());

        post("/product", ((request, response) ->
                productService.createProduct(request.queryParams("name"), Integer.parseInt(request.queryParams("price")))), json());

        after((req, resp) -> {
            resp.type("application/json");
        });
    }
}
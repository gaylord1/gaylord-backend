package controller;

import entities.Product;
import errormapper.ResponseError;
import helper.ParseHelper;
import responseobject.GetOrderDetailsResponse;
import responseobject.OrderInfoResponse;
import service.OrderService;

import java.util.List;

import static spark.Spark.*;
import static utils.JsonUtil.json;


public class OrderController {
    public OrderController(final OrderService orderService) {
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });
        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));

        post("/order", ((request, response) ->
                orderService.addOrder(request.body())), json());

        get("/order/:id", (request, response) -> {
            String id = request.params(":id");
            GetOrderDetailsResponse orderDetails = orderService.getOrderDetails(id);
            if (orderDetails != null)
                return orderDetails;
            response.status(404);
            return new ResponseError("No product with id %s found", id);
        }, json());

        get("/order", (request, response) -> {
            String date = request.queryParams("date");
            List<OrderInfoResponse> orderByDate = orderService.getOrderByDate(date);
            if (orderByDate != null)
                return orderByDate;
            response.status(404);
            return new ResponseError("No product found on this date %s .", date);
        }, json());

        after((req, resp) -> {
            resp.type("application/json");
        });
    }
}
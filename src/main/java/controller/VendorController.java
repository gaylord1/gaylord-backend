package controller;

import entities.Product;
import entities.Vendor;
import errormapper.ResponseError;
import helper.ParseHelper;
import service.VendorService;

import static spark.Spark.*;
import static spark.Spark.after;
import static utils.JsonUtil.json;

public class VendorController {
    public VendorController(final VendorService vendorService) {
//        options("/*",
//                (request, response) -> {
//
//                    String accessControlRequestHeaders = request
//                            .headers("Access-Control-Request-Headers");
//                    if (accessControlRequestHeaders != null) {
//                        response.header("Access-Control-Allow-Headers",
//                                accessControlRequestHeaders);
//                    }
//
//                    String accessControlRequestMethod = request
//                            .headers("Access-Control-Request-Method");
//                    if (accessControlRequestMethod != null) {
//                        response.header("Access-Control-Allow-Methods",
//                                accessControlRequestMethod);
//                    }
//
//                    return "OK";
//                });
//
//        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));

        get("/vendors", ((request, response) -> vendorService.getAllVendors()), json());

        get("/vendor/:id", (request, response) -> {
            String id = request.params(":id");
            if (!ParseHelper.isInteger(id)) {
                response.status(400);
                return new ResponseError("Please enter a valid product id");
            }
            Vendor vendor = vendorService.getVendor(id);
            if (vendor != null)
                return vendor;
            response.status(404);
            return new ResponseError("No product with id %s found", id);
        }, json());

        post("/vendor", ((request, response) ->
                vendorService.addVendor(request.body())), json());

        after((req, resp) -> {
            resp.type("application/json");
        });
    }
}
package logger;

public class Logger {
    public static void LogInfo(String message, Exception e){
        System.out.println("****************** "+message+" ******************");
        e.printStackTrace();
        System.out.println("==================================================");
    }
    public static void LogInfo(String message){
        System.out.println("****************** "+message+" ******************");
    }
}

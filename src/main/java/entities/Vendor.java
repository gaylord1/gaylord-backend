package entities;

import customannotation.DBTable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vendor {
    @DBTable(columnName = "vendor_id")
    private int id;
    @DBTable(columnName = "vendor_name")
    private String name;
    @DBTable(columnName = "address")
    private String address;
    @DBTable(columnName = "phone")
    private String phone;
    @DBTable(columnName = "dob")
    private String dateOfBirth;
}
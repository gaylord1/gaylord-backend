package entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class Order {
    private String order_id;
    private String vendor_name;
    private List<OrderInfo> items;
    private int order_total;
    private String created_at;
    private String updated_at;
}

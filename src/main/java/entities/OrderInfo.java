package entities;

public class OrderInfo {
    private String order_id;
    private String product_name;
    private int product_rate;
    private int quantity_purchased;
    private int quantity_returned;
    private int amount;
}

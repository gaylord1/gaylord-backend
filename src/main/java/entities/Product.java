package entities;

import customannotation.DBTable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {
    @DBTable(columnName = "id")
    private int id;
    @DBTable(columnName = "name")
    private String name;
    @DBTable(columnName = "rate")
    private int price;
}

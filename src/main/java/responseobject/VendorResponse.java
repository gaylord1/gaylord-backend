package responseobject;

import customannotation.DBTable;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VendorResponse {
    private String name;
    private String address;
    private String phone;
    private int age;
}

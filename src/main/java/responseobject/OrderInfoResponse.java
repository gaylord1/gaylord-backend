package responseobject;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OrderInfoResponse {
    private String order_id;
    private String name;
    private String phone_number;
}

package responseobject;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
public class GetOrderDetailsResponse {
    private String order_id;
    private VendorResponse vendor;
    private List<ProductResponse> products;
    private int order_total;
    private String created_at;
    private String updated_at;

}

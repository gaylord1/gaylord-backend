package requestobject;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class OrderRequest {
    private String order_id;
    private String vendor_id;
    private List<ItemDetail> items;

    @Getter
    @Setter
    public static class ItemDetail{
        private int product_id;
        private int qty_purchased;
        private int qty_returned;
    }

    private String created_at;
    private String updated_at;

}

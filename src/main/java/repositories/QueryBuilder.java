package repositories;

import customannotation.DBTable;
import logger.Logger;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class QueryBuilder {
    private ConnectionManager connectionManager;

    public QueryBuilder() {
        this.connectionManager = new ConnectionManager();
    }

    public boolean update(String query) {
        Connection connection = null;
        Statement statement;
        try {
            connection = connectionManager.makeConnectionToDb();
            statement = connection.createStatement();
            int i = statement.executeUpdate(query);
            statement.close();
            if (i == 1)
                return true;
        } catch (Exception e) {
            Logger.LogInfo("Error while adding new product", e);
        } finally {
            connectionManager.releaseConnectionToDb(connection);
        }
        return false;
    }

    public <T> List<T> fetch(Class<T> type, String query) {
        List<T> list = new ArrayList<T>();

        try (Connection conn = connectionManager.makeConnectionToDb();
             Statement stmt = conn.createStatement();
             ResultSet result = stmt.executeQuery(query)) {
            while (result.next()) {
                T t = type.newInstance();
                loadResultSetIntoObject(result, t);
                list.add(t);
            }
        } catch (Exception e) {
            Logger.LogInfo("Error while fetching product details", e);
        }
        return list;
    }


    private void loadResultSetIntoObject(ResultSet rst, Object object)
            throws IllegalArgumentException, IllegalAccessException, SQLException {
        Class<?> zclass = object.getClass();
        for (Field field : zclass.getDeclaredFields()) {
            field.setAccessible(true);
            DBTable column = field.getAnnotation(DBTable.class);
            Object value = rst.getObject(column.columnName());
            Class<?> type = field.getType();
            if (isPrimitive(type)) {
                Class<?> boxed = boxPrimitiveClass(type);
                value = boxed.cast(value);
            }
            field.set(object, value);
        }
    }

    private boolean isPrimitive(Class<?> type) {
        return (type == int.class || type == long.class || type == double.class || type == float.class
                || type == boolean.class || type == byte.class || type == char.class || type == short.class);
    }

    private Class<?> boxPrimitiveClass(Class<?> type) {
        if (type == int.class) {
            return Integer.class;
        } else if (type == long.class) {
            return Long.class;
        } else if (type == double.class) {
            return Double.class;
        } else if (type == float.class) {
            return Float.class;
        } else if (type == boolean.class) {
            return Boolean.class;
        } else if (type == byte.class) {
            return Byte.class;
        } else if (type == char.class) {
            return Character.class;
        } else if (type == short.class) {
            return Short.class;
        } else {
            String string = "class '" + type.getName() + "' is not a primitive";
            throw new IllegalArgumentException(string);
        }
    }
}
package repositories;

import logger.Logger;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionManager {
    private final String url = "jdbc:postgresql://localhost:5432/gaylord_icecream";

    public Connection makeConnectionToDb() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url);
            if (connection != null)
                System.out.println("Connection to db is successful");
        } catch (Exception e) {
            Logger.LogInfo("Error while trying to connect to db", e);
        }
        return connection;
    }

    public void releaseConnectionToDb(Connection connection) {
        try {
            connection.close();
        } catch (Exception e) {
            Logger.LogInfo("Error while trying to close connection to db", e);
        }
    }
}
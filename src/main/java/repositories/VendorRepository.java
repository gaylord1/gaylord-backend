package repositories;

import entities.Product;
import entities.Vendor;
import logger.Logger;

import java.sql.SQLException;
import java.util.List;

public class VendorRepository {
    private QueryBuilder queryBuilder;
    private static final String TABLE_NAME = "vendor_info";

    public VendorRepository() {
        this.queryBuilder = new QueryBuilder();
    }

    public boolean addNewVendor(Vendor vendor) {
        String query = String.format("insert into %s (vendor_name, address, phone, dob) values ('%s', '%s', '%s', '%s');", TABLE_NAME,
                vendor.getName(), vendor.getAddress(), vendor.getPhone(), vendor.getDateOfBirth());
        return queryBuilder.update(query);
    }

    public List<Vendor> getAllVendors() throws SQLException {
        String query = String.format("select * from %s;", TABLE_NAME);
        return queryBuilder.fetch(Vendor.class, query);
    }

    public Vendor getVendor(String id) {
        try {
            String query = String.format("select * from %s where vendor_id='%s';", TABLE_NAME, id);
            List<Vendor> vendors = queryBuilder.fetch(Vendor.class, query);
            if (vendors.size() == 0)
                return null;
            return vendors.get(0);
        }
        catch (Exception e){
            Logger.LogInfo("error while fetching vendor", e);
            return  null;
        }
    }
}

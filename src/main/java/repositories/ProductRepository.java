package repositories;

import entities.Product;
import logger.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductRepository {
    private QueryBuilder queryBuilder;
    private static final String TABLE_NAME = "product_details";

    public ProductRepository() {
        this.queryBuilder = new QueryBuilder();
    }

    public boolean addNewProduct(Product product) {
        String query = String.format("insert into %s (name, rate) values ('%s', %s);", TABLE_NAME, product.getName(),
                product.getPrice());
        return queryBuilder.update(query);
    }

    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        try {
            String query = String.format("select * from %s;", TABLE_NAME);
            products = queryBuilder.fetch(Product.class, query);
            return products;
        }
        catch (Exception e){
            Logger.LogInfo("Error while trying to fetch all products", e);
            return  products;
        }
    }

    public Product getProduct(String id) throws SQLException {
        String query = String.format("select * from %s where id='%s';", TABLE_NAME, id);
        List<Product> products = queryBuilder.fetch(Product.class, query);
        if (products.size() == 0)
            return null;
        return products.get(0);
    }
}
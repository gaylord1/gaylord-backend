package repositories;

import entities.Product;
import entities.Vendor;
import helper.Utils;
import javafx.util.Pair;
import lombok.var;
import requestobject.OrderRequest;
import responseobject.GetOrderDetailsResponse;
import responseobject.OrderInfoResponse;
import responseobject.ProductResponse;
import responseobject.VendorResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class OrderRepository {
    private QueryBuilder queryBuilder;
    private static final String TABLE_NAME = "product_details";
    ProductRepository productRepository;
    VendorRepository vendorRepository;

    List<OrderRequest> orders;

    public OrderRepository() {
        this.queryBuilder = new QueryBuilder();
        orders = new ArrayList<>();
        productRepository = new ProductRepository();
        vendorRepository = new VendorRepository();
    }

    public String addOrder(OrderRequest order) {
        if (orders.add(order))
            return order.getOrder_id();
        return "";
    }

    public GetOrderDetailsResponse getOrderDetails(String orderId) {
        OrderRequest orderDetails = orders.stream().filter(o -> o.getOrder_id().equals(orderId)).findFirst().get();
        Vendor vendor = vendorRepository.getVendor(orderDetails.getVendor_id());
        Pair<List<ProductResponse>, Integer> productInfo = getProductInfo(orderDetails.getItems());
        GetOrderDetailsResponse getOrderDetailsResponse = GetOrderDetailsResponse.builder()
                .order_id(orderId)
                .vendor(getVendorInfo(vendor))
                .products(productInfo.getKey())
                .order_total(productInfo.getValue())
                .created_at(orderDetails.getCreated_at())
                .updated_at(orderDetails.getUpdated_at())
                .build();
        return getOrderDetailsResponse;
    }

    private Pair<List<ProductResponse>, Integer> getProductInfo(List<OrderRequest.ItemDetail> productsOrdered) {
        List<Product> products = productRepository.getAllProducts();
        List<ProductResponse> response = new ArrayList<>();
        int totalAmount = 0;
        for (var orderedItem : productsOrdered) {
            Product product = products.stream().filter(p -> p.getId() == orderedItem.getProduct_id()).findFirst().get();
            int price = getPrice(orderedItem, product.getPrice());
            response.add(ProductResponse.builder()
                    .name(product.getName())
                    .qty_purchase(orderedItem.getQty_purchased())
                    .qty_returned(orderedItem.getQty_returned())
                    .price(price)
                    .build());
            totalAmount += price;
        }
        return new Pair<>(response, totalAmount);
    }

    private VendorResponse getVendorInfo(Vendor vendor) {
        return VendorResponse.builder()
                .name(vendor.getName())
                .address(vendor.getAddress())
                .phone(vendor.getPhone())
                .age(Utils.getAge(vendor.getDateOfBirth()))
                .build();
    }

    private int getPrice(OrderRequest.ItemDetail item, int rate) {
        return (item.getQty_purchased() - item.getQty_returned()) * rate;
    }

    public List<OrderInfoResponse> getOrderByDate(String date) {
        String queriedDate = Utils.getDate(date).toString();
        List<OrderRequest> listOfOrders = orders.stream().filter(o -> o.getCreated_at().equals(queriedDate)).collect(Collectors.toList());
        List<OrderInfoResponse> orderInfos = new ArrayList<>();
        for (OrderRequest o:listOfOrders) {
            Vendor vendor = vendorRepository.getVendor(o.getVendor_id());
            orderInfos.add(OrderInfoResponse.builder()
                    .order_id(o.getOrder_id())
                    .name(vendor.getName())
                    .phone_number(vendor.getPhone())
                    .build());
        }
        return orderInfos;
    }
}
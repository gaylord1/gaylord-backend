package service;

import helper.Utils;
import repositories.OrderRepository;
import requestobject.OrderRequest;
import responseobject.GetOrderDetailsResponse;
import responseobject.OrderInfoResponse;
import transformer.GsonTransformer;

import java.util.List;
import java.util.UUID;

public class OrderService {
    private OrderRepository repository;

    public OrderService() {
        this.repository = new OrderRepository();
    }

    public String addOrder(String payload){
        String orderTime = Utils.getCurrentDateTime();
        OrderRequest requestPayload = GsonTransformer.toJson(OrderRequest.class, payload);
        requestPayload.setOrder_id(UUID.randomUUID().toString());
        requestPayload.setCreated_at(orderTime);
        requestPayload.setUpdated_at(orderTime);
        return repository.addOrder(requestPayload);
    }

    public GetOrderDetailsResponse getOrderDetails(String orderId){
       return repository.getOrderDetails(orderId);
    }

    public List<OrderInfoResponse> getOrderByDate(String date){
        return repository.getOrderByDate(date);
    }
}

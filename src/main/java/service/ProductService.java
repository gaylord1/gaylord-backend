package service;

import entities.Product;
import repositories.ProductRepository;

import java.sql.SQLException;
import java.util.List;

public class ProductService {
    private ProductRepository repository;

    public ProductService() {
        this.repository = new ProductRepository();
    }

    public List<Product> getAllProducts() throws SQLException {
        return repository.getAllProducts();
    }

    public Product getProduct(String id) throws SQLException {
           return repository.getProduct(id);
    }

    public Product createProduct(String name, int price){
        Product p = new Product();
        p.setName(name);
        p.setPrice(price);
        repository.addNewProduct(p);
        return p;
    }

    public Product updateProduct(String productId, String name, long price){
        //implementation
        return null;
    }

    public boolean deleteProduct(String productId){
        //impelemnation
        return true;
    }
}

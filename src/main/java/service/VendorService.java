package service;

import entities.Product;
import entities.Vendor;
import repositories.VendorRepository;
import transformer.GsonTransformer;

import java.sql.SQLException;
import java.util.List;

public class VendorService {
    private VendorRepository repository;

    public VendorService() {
        this.repository = new VendorRepository();
    }

    public List<Vendor> getAllVendors() throws SQLException {
        return repository.getAllVendors();
    }

    public Vendor getVendor(String id) throws SQLException {
        return repository.getVendor(id);
    }

    public Vendor addVendor(String vendorBody) {
        Vendor vendor = GsonTransformer.toJson(Vendor.class, vendorBody);
        if (!repository.addNewVendor(vendor))
            return null;
        return vendor;
    }
}
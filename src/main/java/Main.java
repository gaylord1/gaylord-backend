import controller.OrderController;
import controller.ProductController;
import controller.VendorController;
import service.OrderService;
import service.ProductService;
import service.VendorService;

public class Main {
    public static void main(String[] args) {
        new ProductController(new ProductService());
        new VendorController(new VendorService());
        new OrderController(new OrderService());
    }
}
